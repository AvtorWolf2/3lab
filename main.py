class Bank(object):

    def __init__(self, summ, period, perc):
        self.summ = summ
        self.period = period
        self.perc = perc
#дифференциорованные платежи
    def diff_int(self):
        arr = []
        mp_cnt = self.period * 12
        rest = self.summ
        mp_real = self.summ / (self.period * 12.0)
        while mp_cnt != 0:
            mp = mp_real + (rest * self.perc / 1200)
            arr.append(round(mp, 2))
            rest = rest - mp_real
            mp_cnt = mp_cnt - 1
        return arr, round(sum(arr), 2)

# аннуитентые платежи
    def ann_int(self):
        mp_cnt = self.period * 12
        r = self.perc / 1200.0
        ak = (r * (1 + r) ** mp_cnt) / (((1 + r) ** mp_cnt) - 1)
        mp = self.summ * ak
        total = mp * mp_cnt
        return round(mp, 2), round(total, 2)


test_obj = Bank(1000, 10, 15)
print(test_obj.ann_int())
print(test_obj.diff_int())